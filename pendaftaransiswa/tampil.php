<?php
	$conn = mysqli_connect('localhost','root','','siswa');
	
	if(!$conn){
		die("Connection failed : ".mysqli_connect_error());
	}
	
	$query = "select *from t_siswa";
	$hasil = mysqli_query($conn, $query);
	$data = array();
	while($temp = mysqli_fetch_array($hasil)){
		$data[] = $temp;
	}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Daftar Siswa</title>
</head>
<body>
<h1 style="text-align:center">Daftar Siswa</h1><br>
<center>
<table border="1px" cellpadding="5px" width="90%" >
	<tr style="text-align:center">
		<th>No</th>
		<th>NIS</th>
		<th>Nama</th>
		<th>Tanggal Lahir</th>
		<th>Jenis Kelamin</th>
		<th>No. Telepon</th>
		<th>Alamat</th>
	</tr>
	
	<?php
		$i=0;
		foreach($data as $row){
			$i++;
		
	?>
	<tr>
		<td><?php echo $i; ?></td>
		<td><?php echo $row['nis']; ?></td>
		<td><?php echo $row['nama']; ?></td>
		<td><?php echo $row['tgl']; ?></td>
		<td><?php echo $row['jk']; ?></td>
		<td><?php echo $row['telepon']; ?></td>
		<td><?php echo $row['alamat']; ?></td>
	</tr>
	<?php } ?>
</table>
</center>
</body>
</html>