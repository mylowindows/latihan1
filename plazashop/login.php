<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Gayathri&display=swap" rel="stylesheet">
<title>Login</title>
<style>
.bg{
	width:350px;
	background:lightgrey;
	margin : 80px auto;
	padding : 30px 20px;
}
.h1{
	text-align:center; 
	margin-bottom:30px; 
	margin-top:10%;
	font-family:gayathri;
	padding-top:60px
}
.label1{
	margin-right:20px
}
.label2{
	margin-top:10px;
	margin-right:25px
}
.button{
	margin-top:20px;
	margin-left:40%;
	padding-bottom:60px
	
}
.button2{
	height:30px
}
.container{
	background-color:rgba(0,0,0,0.2)
}
label{
	font-family:gayathri
}
</style>
</head>
<body class="bg">
<div class="container">
<h1 class="h1">Login</h1>
<form action="aksi.php" method="POST">
    <label class="label1">Username </label>
		<input type="text" name="username"><br>
	<label class="label2">Password </label>
		<input type="password" name="password"><br>
	<div class="button">
	<button type="submit" class="btn btn-outline-primary button2">Submit</button> <button type="submit" class="btn btn-outline-danger button2">Reset</button>
	</div>
</form>
</div>
</body>
</html>